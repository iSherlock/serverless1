import React, { Component } from 'react';
import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://u42v00h7id.execute-api.us-east-1.amazonaws.com/default/library";


class Authors extends Component {
  
  addAuthor(ev){
    
  }
  
   render() {


    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="aid">AID</label>
                <input type="number" className="form-control" id="aid" defaultValue="1" required />
                <div className="invalid-feedback">
                    An AID is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="fname">First Name</label>
                <input type="text" className="form-control" id="fname" defaultValue="" required />
                <div className="invalid-feedback">
                    A first name is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="lname">Last Name</label>
                <input type="text" className="form-control" id="lname" defaultValue="100" required />
                <div className="invalid-feedback">
                    The last name is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add author</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">AID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Authors;
