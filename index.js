var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "books";

// need for cross-site POST to work
const corsHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'content-type'
};

async function newBook(book) {
    book.isbn = parseInt(book.isbn);
    book.pages = parseInt(book.pages);
    let params = {
        TableName: table,
        Item: book
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

exports.handler = async (event) => {
    // sample event properties:
    // event.path: '/default/library/books'
    // event.httpMethod: 'POST'
    // event.body: json encoded object

    console.log( event.path );
    console.log( event.httpMethod );
    let body = JSON.parse(event.body);
    console.log( body );
    
    if ( event.httpMethod == 'POST' && event.path == '/default/library/books') {
        return newBook(body);
    }
    
    if ( event.httpMethod == 'GET' && event.path == '/default/library/books') {
        return table;
    }
    
    const response = {
        statusCode: 200,
        headers: corsHeaders,
        body: JSON.stringify('Unrecognized request'),
    };
    return response;
};